from typing import Generic, Iterable, List, Literal, TypeVar
import beziers.line
import beziers.point
import beziers.path
import beziers.cubicbezier
import beziers.path.geometricshapes
from pprint import pprint
import math

from paths import *

LockMode = Literal['center'] | Literal['lhs'] | Literal['rhs']
DimMode = Literal['x'] | Literal['y'] | Literal['xy']

Pe = TypeVar('Pe', PathElement, Point)


class StrokeElement(Generic[Pe]):

    def __init__(
            self,
            point: Pe,
            width: float,
            angle: float = 0,
            fixation: float = 0,
            lock: LockMode = 'center',
            dim_by: DimMode = 'xy',
            contraction_factor: float = math.sqrt(0.5),
            synthetic=False,
    ) -> None:
        self.point = point
        self.width = width
        self.angle = angle
        self.fixation = fixation
        self.lock = lock
        self.dim_by = dim_by
        # Offsetting a Bézier curve is surprisingly difficult (given its locking functionality), so the stroking code doesn't use the "correct" approach for offsetting.
        # Instead, it just scales the offsets of the control points according to the curvatures at the end. How much they are scaled by depends on this parameter.
        self.contraction_factor = contraction_factor
        # This flag is set if this point has been interpolated from a curve.
        self.synthetic = synthetic

    def transform_angle(self, t: float) -> float:
        return mix_angle(self.fixation, t - math.pi / 2, self.angle)

    def offsets(self, t: float) -> LineSegment:
        tt = self.transform_angle(t)
        factor = 1 / math.cos(tt - (t - math.pi / 2))
        offset = Point.polar(factor * self.width / 2, tt)
        return LineSegment(-offset, offset)

    def offset_at_angle(self, t: float, p: Point) -> LineSegment:
        return self.offsets(t) + p


def tangent_on_a(a: PathElement, b: PathElement) -> LineSegment:
    if isinstance(b, CurveControl):
        bp = b.cp1
    else:
        bp = b
    return LineSegment(a.dest_point(), bp)


def tangent_on_b(a: PathElement, b: PathElement) -> LineSegment:
    if isinstance(b, CurveControl):
        ap = b.cp2
    else:
        ap = a.dest_point()
    return LineSegment(ap, b.dest_point())


def cubic_bezier_inv_curvature(p0: Point, p1: Point, p2: Point, p3: Point,
                               t: float) -> float:
    # k(t) = (x'' y' - y'' x') / (x'^2 + y'^2)^(3/2)
    d = (p1 - p0).scale(3 * (1 - t)**2) + (p2 - p1).scale(
        6 * (1 - t) * t) + (p3 - p2).scale(3 * t**2)
    dd = (p2 - p1.scale(2) + p0).scale(
        6 * (1 - t)) + (p3 - p2.scale(2) + p1).scale(6 * t)
    denom = dd.x * d.y - dd.y * d.x
    num = (d.x**2 + d.y**2)**(3 / 2)
    if denom == 0:
        print("Warning: radius of curvature is infinite!", file=stderr)
        print(f"p0 = {p0}", file=stderr)
        print(f"p1 = {p1}", file=stderr)
        print(f"p2 = {p2}", file=stderr)
        print(f"p3 = {p3}", file=stderr)
        print(f"t = {t}", file=stderr)
        return math.inf
    return num / denom


def inv_curvature_on_a(a: PathElement, b: PathElement) -> float:
    if isinstance(b, CurveControl):
        return cubic_bezier_inv_curvature(a.dest_point(), b.cp1, b.cp2, b.dest,
                                          0)
    else:
        return 0


def inv_curvature_on_b(a: PathElement, b: PathElement) -> float:
    if isinstance(b, CurveControl):
        return cubic_bezier_inv_curvature(a.dest_point(), b.cp1, b.cp2, b.dest,
                                          1)
    else:
        return 0


def angle_from_a(a: PathElement, b: PathElement) -> float:
    t = tangent_on_a(a, b)
    return (t.p2 - t.p1).angle()


def angle_to_b(a: PathElement, b: PathElement) -> float:
    t = tangent_on_b(a, b)
    return (t.p2 - t.p1).angle()


def filter_dims(v: Point, d: DimMode) -> Point:
    if d == 'xy': return v
    if d == 'x': return Point(v.x, 0)
    if d == 'y': return Point(0, v.y)


def adjust_points(pl: List[PathElement], offsets: List[Point],
                  cf: List[float]) -> None:
    orig_points = pl[:]
    sw_curv_radii = [
        inv_curvature_on_a(p1, p2) for p1, p2 in itertools.pairwise(pl)
    ]
    ew_curv_radii = [
        inv_curvature_on_b(p1, p2) for p1, p2 in itertools.pairwise(pl)
    ]
    sw_angles = [angle_from_a(p1, p2) for p1, p2 in itertools.pairwise(pl)]
    ew_angles = [angle_to_b(p1, p2) for p1, p2 in itertools.pairwise(pl)]
    for i in range(len(pl)):
        p = pl[i]
        offset = offsets[i]
        if isinstance(p, Point):
            pl[i] = pl[i] + offset
        else:
            pl[i] = CurveControl(p.cp1, p.cp2 + offset, p.dest + offset)
        if i < len(pl) - 1 and isinstance(pl[i + 1], CurveControl):
            next = pl[i + 1]
            assert isinstance(next, CurveControl)
            pl[i + 1] = CurveControl(next.cp1 + offset, next.cp2, next.dest)

    for i in range(len(pl) - 1):
        a = pl[i]
        b = pl[i + 1]
        oa = orig_points[i]
        ob = orig_points[i + 1]
        if isinstance(b, CurveControl):
            inward_radius_a = offsets[i].abs() * math.cos(offsets[i].angle() -
                                                          (sw_angles[i] -
                                                           math.pi / 2))
            inward_radius_b = offsets[i].abs() * math.cos(
                offsets[i + 1].angle() - (ew_angles[i] - math.pi / 2))
            outward_radius_a = sw_curv_radii[i]
            outward_radius_b = ew_curv_radii[i]
            # I don't know why I need to multiply by 1/sqrt(2), but here it is.
            assert isinstance(ob, CurveControl)
            new_cp1 = a.dest_point() + (ob.cp1 - oa.dest_point()).scale(
                1 - cf[i] * inward_radius_a / outward_radius_a)
            new_cp2 = b.dest + (ob.cp2 - ob.dest).scale(
                1 - cf[i + 1] * inward_radius_b / outward_radius_b)
            pl[i + 1] = CurveControl(new_cp1, new_cp2, b.dest)


def stroke_character(pen: MyPen,
                     points: List[StrokeElement[PathElement]],
                     max_iters=100,
                     debug_stroke=False):
    assert len(points) >= 2
    assert max_iters >= 1
    point_locations = [p.point for p in points]
    cf = [p.contraction_factor for p in points]
    for _ in range(max_iters):
        lw_offs = []
        rw_offs = []
        # First point
        s = points[0].offsets(
            angle_from_a(point_locations[0], point_locations[1]))
        lw_offs.append(s.p1)
        rw_offs.append(s.p2)
        # Middle points
        for i in range(1, len(points) - 1):
            co = points[i]
            p, c, n = point_locations[i - 1:i + 2]
            t1 = tangent_on_a(c, n)
            t2 = tangent_on_b(p, c)
            o1 = Point.polar(co.width / 2,
                             co.transform_angle(angle_from_a(c, n)))
            o2 = Point.polar(co.width / 2, co.transform_angle(angle_to_b(p,
                                                                         c)))
            i1 = (t1 - o1).intersection_point(t2 - o2,
                                              require_intersection=False)
            i2 = (t1 + o1).intersection_point(t2 + o2,
                                              require_intersection=False)
            if i1 is None:
                k1 = -o2
            else:
                k1 = i1 - c.dest_point()
            if i2 is None:
                k2 = o2
            else:
                k2 = i2 - c.dest_point()
            lw_offs.append(k1)
            rw_offs.append(k2)
        # Last point
        s = points[-1].offsets(
            angle_to_b(point_locations[-2], point_locations[-1]))
        lw_offs.append(s.p1)
        rw_offs.append(s.p2)
        lw_points = point_locations[:]
        rw_points = point_locations[:]
        adjust_points(lw_points, lw_offs, cf)
        adjust_points(rw_points, rw_offs, cf)
        moved_point = False
        point_offsets = [Point(0, 0)] * len(point_locations)
        for i, po in enumerate(points):
            if po.lock == 'lhs':
                diff = filter_dims(
                    po.point.dest_point() - lw_points[i].dest_point(),
                    po.dim_by)
                if diff.abs() >= 1e-5:
                    point_offsets[i] = diff
                    moved_point = True
            elif po.lock == 'rhs':
                diff = filter_dims(
                    po.point.dest_point() - rw_points[i].dest_point(),
                    po.dim_by)
                if diff.abs() >= 1e-5:
                    point_offsets[i] = diff
                    moved_point = True
        adjust_points(point_locations, point_offsets, cf)
        if not moved_point: break
    else:
        print(
            f"stroke_character warning: failed to converge within {max_iters} iterations",
            file=stderr)
    if debug_stroke:
        for p in point_locations:
            pen.add(p)
        pen.close_path()
        return
    for p in lw_points:
        pen.add(p)
    for p in reverse_path_elements(rw_points):
        pen.add(p)
    pen.close_path()


# def interpolate_stroked_bezier_curve(po0: StrokeElement[PathElement],
#                                      p1: Point, p2: Point,
#                                      po3: StrokeElement[PathElement]):
#     p0 = po0.point.dest_point()
#     p3 = po3.point.dest_point()
