from abc import ABC, abstractmethod
import itertools
from sys import stderr
from typing import List, Literal, Optional, Tuple
from typing_extensions import Self
import fontforge
import math


class PathElementABC(ABC):

    @abstractmethod
    def transform(self, mat) -> Self:
        pass

    @abstractmethod
    def add_point_to_pen(self, pen: fontforge.glyphPen):
        pass

    @abstractmethod
    def dest_point(self):
        pass


class Point(PathElementABC):

    def __init__(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

    def __str__(self) -> str:
        return f"Point({self.x}, {self.y})"

    def __repr__(self) -> str:
        return f"Point({self.x}, {self.y})"

    @classmethod
    def polar(cls, r: float, theta: float) -> Self:
        return Point(r * math.cos(theta), r * math.sin(theta))

    def round(self) -> Self:
        return Point(round(self.x), round(self.y))

    def scale(self, sx: float, sy: float = None) -> Self:
        if sy is None: sy = sx
        return Point(sx * self.x, sy * self.y)

    def to_rpair(self) -> Tuple[int, int]:
        return (round(self.x), round(self.y))

    def abs(self) -> float:
        return math.sqrt(self.x * self.x + self.y * self.y)

    def __add__(self, other: Self) -> Self:
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other: Self) -> Self:
        return Point(self.x - other.x, self.y - other.y)

    def __neg__(self) -> Self:
        return Point(-self.x, -self.y)

    def transform(self, mat) -> Self:
        tx = mat[0] * self.x + mat[2] * self.y + mat[4]
        ty = mat[1] * self.x + mat[3] * self.y + mat[5]
        return Point(tx, ty)

    def angle(self) -> float:
        return math.atan2(self.y, self.x)

    def add_point_to_pen(self, pen: fontforge.glyphPen):
        pen.lineTo(self.to_rpair())

    def dest_point(self) -> Self:
        return self


class CurveControl(PathElementABC):

    def __init__(self, cp1: Point, cp2: Point, dest: Point) -> None:
        self.cp1 = cp1
        self.cp2 = cp2
        self.dest = dest

    def __str__(self) -> str:
        return f"CurveControl({self.cp1}, {self.cp2}, {self.dest})"

    def __repr__(self) -> str:
        return f"CurveControl({self.cp1}, {self.cp2}, {self.dest})"

    def __add__(self, other: Point) -> Self:
        return CurveControl(self.cp1 + other, self.cp2 + other,
                            self.dest + other)

    def __sub__(self, other: Point) -> Self:
        return CurveControl(self.cp1 - other, self.cp2 - other,
                            self.dest - other)

    def to_rpair(self):
        return (*self.cp1.to_rpair(), *self.cp2.to_rpair(),
                *self.dest.to_rpair())

    def add_point_to_pen(self, pen: fontforge.glyphPen):
        pen.curveTo(*self.to_rpair())

    def transform(self, mat) -> Self:
        return CurveControl(self.cp1.transform(mat), self.cp2.transform(mat),
                            self.dest.transform(mat))

    def dest_point(self) -> Point:
        return self.dest


PathElement = Point | CurveControl


def reverse_path_elements(points: List[PathElement]) -> List[PathElement]:
    result = []
    cps = None

    def push(p):
        nonlocal cps
        if cps:
            pp = CurveControl(cps[1], cps[0], p)
        else:
            pp = p
        result.append(pp)
        cps = None

    for point in reversed(points):
        if isinstance(point, Point):
            push(point)
        elif isinstance(point, CurveControl):
            push(point.dest)
            cps = (point.cp1, point.cp2)
        else:
            raise AssertionError(f"Unknown type for {point}")
    return result


class MyPen:

    def __init__(self, underlying: fontforge.glyphPen) -> None:
        self.underlying = underlying
        self.current_contour = []

    def add(self, p: PathElement):
        self.current_contour.append(p)

    def line_to(self, x: float, y: float) -> None:
        self.current_contour.append(Point(x, y))

    def linep_plus_angle(self, angle_offset: float, dist: float) -> None:
        a = self.current_contour[-2].dest_point()
        b = self.current_contour[-1].dest_point()
        prev_angle = math.atan2(b.y - a.y, b.x - a.x)
        new_angle = prev_angle + angle_offset
        self.line_to(b.x + dist * math.cos(new_angle),
                     b.y + dist * math.sin(new_angle))

    def linep_plus_angle_2x(self, angle_offset: float,
                            target_x: float) -> None:
        a = self.current_contour[-2].dest_point()
        b = self.current_contour[-1].dest_point()
        prev_angle = math.atan2(b.y - a.y, b.x - a.x)
        new_angle = prev_angle + angle_offset
        dist = (target_x - b.x) / math.cos(new_angle)
        self.line_to(target_x, b.y + dist * math.sin(new_angle))

    def linep_plus_angle_2y(self, angle_offset: float,
                            target_y: float) -> None:
        a = self.current_contour[-2].dest_point()
        b = self.current_contour[-1].dest_point()
        prev_angle = math.atan2(b.y - a.y, b.x - a.x)
        new_angle = prev_angle + angle_offset
        dist = (target_y - b.y) / math.sin(new_angle)
        self.line_to(b.x + dist * math.cos(new_angle), target_y)

    def curve_to(self, c1x, c1y, c2x, c2y, x: float, y: float) -> None:
        self.current_contour.append(
            CurveControl(Point(c1x, c1y), Point(c2x, c2y), Point(x, y)))

    def open_path(self) -> None:
        self.underlying.moveTo(*self.current_contour[0].to_rpair())
        for p in self.current_contour[1:]:
            p.add_point_to_pen(self.underlying)
        self.current_contour = []

    def close_path(self) -> None:
        self.open_path()
        self.underlying.closePath()

    def close_path_reverse(self) -> None:
        self.current_contour = reverse_path_elements(self.current_contour)
        self.close_path()


class LineSegment:

    def __init__(self, p1: Point, p2: Point) -> None:
        self.p1 = p1
        self.p2 = p2

    def __str__(self) -> str:
        return f"LineSegment({self.p1}, {self.p2})"

    def __repr__(self) -> str:
        return f"LineSegment({self.p1}, {self.p2})"

    def __add__(self, other: Point) -> Self:
        return LineSegment(self.p1 + other, self.p2 + other)

    def __sub__(self, other: Point) -> Self:
        return LineSegment(self.p1 - other, self.p2 - other)

    def intersection_point(
            self,
            other: Self,
            require_intersection: bool = True) -> Optional[Point]:
        # at + bu = c,
        # dt + eu = f
        a = self.p2.x - self.p1.x
        b = other.p1.x - other.p2.x
        c = other.p1.x - self.p1.x
        d = self.p2.y - self.p1.y
        e = other.p1.y - other.p2.y
        f = other.p1.y - self.p1.y
        try:
            # Cancel out u-terms:
            # aet + beu = ce
            # -bdt - beu = -bf
            # ----------------
            # t (ae - bd) = ce - bf
            t = (c * e - b * f) / (a * e - b * d)
            # Cancel out t-terms:
            # adt + bdu = cd
            # -adt - aeu = -af
            # ----------------
            # u (bd - ae) = cd - af
            u = (c * d - a * f) / (b * d - a * e)
            if 0 <= t <= 1 and 0 <= u <= 1 or not require_intersection:
                return self.p1 + (self.p2 - self.p1).scale(t)
            return None
        except ZeroDivisionError:
            return None


class Polygon:

    def __init__(self, points: List[Point]) -> None:
        self.points = points

    def __str__(self) -> str:
        return f"Polygon({self.points})"

    def __repr__(self) -> str:
        return f"Polygon({self.points})"

    def __getitem__(self, idx: int) -> Point:
        return self.points[idx]

    def line_segment(self, idx: int) -> LineSegment:
        return LineSegment(self.points[idx],
                           self.points[(idx + 1) % len(self.points)])

    def transform(self, mat) -> Self:
        return Polygon([p.transform(mat) for p in self.points])

    def translate(self, x: float, y: float) -> Self:
        return Polygon([Point(p.x + x, p.y + y) for p in self.points])

    def scale(self, x: float, y: float) -> Self:
        return Polygon([p.scale(x, y) for p in self.points])

    @classmethod
    def rectangle(cls, x1: float, x2: float, y1: float, y2: float) -> Self:
        return Polygon(
            [Point(x1, y1),
             Point(x1, y2),
             Point(x2, y2),
             Point(x2, y1)])

    def trace_with_pen(self, pen: MyPen) -> None:
        for p in self.points:
            pen.line_to(*p.to_rpair())
        pen.close_path()

    def normalize_x(self) -> Self:
        offset = -min(p.x for p in self.points)
        return Polygon([Point(p.x + offset, p.y) for p in self.points])


# REQUIREMENTS FOR STROKING FUNCTIONALITY:
#
# * handle both corner and curve points
# * specify width at each point
# * different end types (e.g. "square" ends such as those in o, î &c., as well as "oblique" ends such as those in þ, f, j &c., and later, "arrowhead" ends for punctuation)
#
# useful sources:
#
# * https://math.stackexchange.com/questions/3276910/cubic-b%C3%A9zier-radius-of-curvature-calculation


def mix_angle(t: float, a: float, b: float) -> float:
    diff = (b - a) % (2 * math.pi)
    if diff < math.pi:
        return a + diff * t
    else:
        return a + (-2 * math.pi + diff) * t
