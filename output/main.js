let output = document.getElementById("font-output");
let input = document.getElementById("font-input");
let outputKern = document.getElementById("font-kerning");

const ORIGIN = 0x10C80;

const CENVOS_SUBSTITUTION_TABLE = (function () {
  let request = new XMLHttpRequest();
  request.open("GET", "./mapping.json", false);
  request.send();
  return JSON.parse(request.responseText);
})();

function latinToCenvos(latin) {
  return latin.replaceAll("+*", "×")
    .split("")
    .map((a) => CENVOS_SUBSTITUTION_TABLE[a] || a)
    .join("");
}
const ZWJ = latinToCenvos("~");

function generateKerningTable() {
  let kernables1 = [];
  let kernables2 = [];
  for (let i = (ORIGIN + 0x00); i < (ORIGIN + 0x1E); ++i) {
    let k = String.fromCodePoint(i);
    kernables1.push(k);
    kernables2.push(k);
  }
  for (let i = (ORIGIN + 0x1E); i < (ORIGIN + 0x22); ++i) {
    let k = String.fromCodePoint(i);
    kernables2.push(k);
  }
  for (let i = (ORIGIN + 0x23); i < (ORIGIN + 0x2B); ++i) {
    let k = String.fromCodePoint(i);
    kernables1.push(k);
    kernables2.push(k);
  }
  kernables2.push(latinToCenvos("{"));
  kernables2.push(latinToCenvos("&"));
  for (let i = (ORIGIN + 0x40); i < (ORIGIN + 0x45); ++i) {
    let k = String.fromCodePoint(i);
    kernables1.push(k);
    kernables2.push(k);
    kernables1.push(k + k);
    kernables2.push(k + k);
  }
  for (let s of ["ee", "em", "me", "mm", "jâ", "âj"].map(latinToCenvos)) {
    kernables1.push(s);
    kernables2.push(s);
  }
  kernables2 = kernables2.map(a => a + ZWJ);
  kernables2.push(latinToCenvos("c"));
  kernables2.push(latinToCenvos("ŋ"));
  // No danger of XSS, since the strings we're interpolating don't have any of the HTML special characters.
  html = "<table lang=\"art-x-ncs-v9-cenvos\"><thead><tr><th></th>";
  for (let c2 of kernables2) {
    html += `<th>${c2}</th>`;
  }
  html += "</tr></thead><tbody>";
  for (let c1 of kernables1) {
    html += `<tr><th>${c1 + ZWJ}</th>`;
    for (let c2 of kernables2) {
      html += `<td>${c1 + c2}</td>`;
    }
    html += "</tr>";
  }
  html += "</tbody></table>";
  outputKern.innerHTML = html;
}

input.addEventListener(
  'input', function (event) {
    output.innerText = latinToCenvos(this.value);
  });

output.innerText = latinToCenvos(input.value);
generateKerningTable();
