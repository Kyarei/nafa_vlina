#!/usr/bin/env python3

from abc import ABC, abstractmethod
from typing import Dict, List, Optional, Self, Tuple
import fontforge
import math
import psMat
import os
import json

from paths import *
from stroking import *

ROMANIZATION = list(
    "cenŋvosþšrlłmafgptčîjidðhħêôâu.;?!~-#+×@*’·{}«»&0123456789ABCDEFwxyz/:")

# Formerly, the characters were mapped to the Private Use Area, but this caused
# problems as the PUA characters are considered LTR characters.
# The mapping has been changed to the Old Hungarian block because it is right
# to left. It is also large enough for all our glyphs, unlike other blocks such
# as Manichaean or Avestan.
# (Note that the Joining_Type property is irrelevant, since we do not use the
# `fina`` feature for final c and ŋ.)
#
# TODO: generate separate Graphite-enabled and fallback versions of fonts
ORIGIN = 0x10C80

PPIX = 128
ASCENT = 7 * PPIX
DESCENT = 3 * PPIX
XHEIGHT = 4.5 * PPIX
MARKER_BASELINE = 4.5 * PPIX
O_HEIGHT = 15 * PPIX / 4

WEDGE_PEAK_THICKNESS_MULTI = 0.875
WEDGE_PEAK_PLUS_LINE_THICKNESS_MULTI = 0.75
RL_EYE_THICKNESS_MULTI = 0.625

OVERSHOOT_ANGULAR = 5 * PPIX / 32
OVERSHOOT_ROUND = PPIX / 8

ARROWHEAD_LENGTH = PPIX * 1.8
ARROWHEAD_HALF_WIDTH = PPIX

RIGHT90D = math.radians(-90)
LEFT90D = math.radians(90)

MARGIN_BASE = PPIX // 2
MARGIN = round(0.9 * MARGIN_BASE)
MARGIN_DIAG = round(0.54 * MARGIN_BASE)
MARGIN_ROUND = round(0.75 * MARGIN_BASE)
MARGIN_HORIZ = round(0.58 * MARGIN_BASE)
MARGIN_ARROW = round(0.45 * MARGIN_BASE)
MARGIN_LARGE = round(1.2 * MARGIN_BASE)

MARGINSPEC = {
    '|': MARGIN,
    '/': MARGIN_DIAG,
    'x': MARGIN_DIAG,  # might be set to a different value in the future
    '(': MARGIN_ROUND,
    '-': MARGIN_HORIZ,
    '<': MARGIN_ARROW,
    '=': MARGIN_LARGE,
    '.': 0,
}


def vertical(x: float, y1: float, y2: float) -> Polygon:
    return Polygon.rectangle(x, x + PPIX, y1, y2)


def horizontal(x1: float, x2: float, y: float) -> Polygon:
    return Polygon.rectangle(x1, x2, y, y + PPIX)


def vertical_line(pen, x: float, y1: float, y2: float):
    vertical(x, y1, y2).trace_with_pen(pen)


def horizontal_line(pen, x1: float, x2: float, y: float):
    horizontal(x1, x2, y).trace_with_pen(pen)


def draw_wedge(pen: MyPen,
               width: float,
               thickness: float = 1,
               base_adj: float = 0,
               inverted: bool = False):
    if inverted:
        base = XHEIGHT
        asc = -OVERSHOOT_ANGULAR
        asc2 = asc + 2 * PPIX - base_adj
    else:
        base = 0
        asc = XHEIGHT + OVERSHOOT_ANGULAR
        asc2 = asc - 2 * PPIX + base_adj
    pen.line_to(0, base)
    pen.line_to(width * PPIX, asc)
    pen.line_to(2 * width * PPIX, base)
    pen.line_to((2 * width - thickness) * PPIX, base)
    pen.line_to(width * PPIX, asc2)
    pen.line_to(thickness * PPIX, base)
    if inverted:
        pen.close_path_reverse()
    else:
        pen.close_path()


def draw_oval(pen: MyPen,
              min_x: float,
              max_x: float,
              min_y: float,
              max_y: float,
              thickness: float = PPIX):
    avg_x = (min_x + max_x) / 2
    avg_y = (min_y + max_y) / 2
    ho = (max_x - min_x) / 4
    vo = (max_y - min_y) / 4
    pen.line_to(avg_x, min_y)
    pen.curve_to(avg_x - ho, min_y, min_x, avg_y - vo, min_x, avg_y)
    pen.curve_to(min_x, avg_y + vo, avg_x - ho, max_y, avg_x, max_y)
    pen.curve_to(avg_x + ho, max_y, max_x, avg_y + vo, max_x, avg_y)
    pen.curve_to(max_x, avg_y - vo, avg_x + ho, min_y, avg_x, min_y)
    pen.close_path()
    ho = (max_x - min_x - 2 * thickness) / 4
    vo = (max_y - min_y - 2 * thickness) / 4
    pen.line_to(avg_x, min_y + thickness)
    pen.curve_to(avg_x + ho, min_y + thickness, max_x - thickness, avg_y - vo,
                 max_x - thickness, avg_y)
    pen.curve_to(max_x - thickness, avg_y + vo, avg_x + ho, max_y - thickness,
                 avg_x, max_y - thickness)
    pen.curve_to(avg_x - ho, max_y - thickness, min_x + thickness, avg_y + vo,
                 min_x + thickness, avg_y)
    pen.curve_to(min_x + thickness, avg_y - vo, avg_x - ho, min_y + thickness,
                 avg_x, min_y + thickness)
    pen.close_path()


def draw_dot(pen: MyPen, x: float, y: float) -> None:
    pen.line_to(x, y + PPIX)
    pen.line_to(x + PPIX, y + PPIX)
    pen.line_to(x + PPIX, y)
    pen.line_to(x, y)
    pen.close_path()


def draw_arrowhead(pen: MyPen, x: float, y: float, angle: float) -> None:
    c, s = math.cos(angle), math.sin(angle)
    pen.line_to(x + ARROWHEAD_LENGTH * c, y + ARROWHEAD_LENGTH * s)
    pen.line_to(x + ARROWHEAD_HALF_WIDTH * s, y - ARROWHEAD_HALF_WIDTH * c)
    pen.line_to(x - ARROWHEAD_HALF_WIDTH * s, y + ARROWHEAD_HALF_WIDTH * c)
    pen.close_path()


def subtract_mask_layer(glyph_obj):
    glyph_obj.activeLayer = "mask"  # Just to be safe
    glyph_obj.exclude(glyph_obj.layers[1])
    glyph_obj.activeLayer = 1
    glyph_obj.clear(1)
    glyph_obj.layers[1] = glyph_obj.layers["mask"]
    glyph_obj.clear("mask")


def ff_stroke(glyph_obj, remove_overlap: bool = True):
    glyph_obj.stroke("circular",
                     PPIX,
                     "butt",
                     "miter",
                     extrema=True,
                     removeoverlap="layer" if remove_overlap else "none")


class MyGlyph(ABC):

    def __init__(self, codepoint: int, name: Optional[str],
                 width: Optional[int], margins: Tuple[int, int]) -> None:
        self.codepoint = codepoint
        self.name = name
        self.width = width
        self.margins = margins

    @abstractmethod
    def draw_on_glyph(self, glyph_obj: fontforge.glyph) -> None:
        pass

    def generate(self, font: fontforge.font) -> None:
        if self.name:
            glyph_obj = font.createChar(self.codepoint, self.name)
        else:
            glyph_obj = font.createChar(self.codepoint)
        self.draw_on_glyph(glyph_obj)
        # glyph_obj.correctDirection()
        glyph_obj.removeOverlap()
        glyph_obj.round()
        bb = glyph_obj.boundingBox()
        width = self.width
        if width is None:
            width = math.ceil(bb[2] - bb[0])
            glyph_obj.transform(psMat.translate(self.margins[0] - bb[0], 0))
        else:
            glyph_obj.transform(psMat.translate(self.margins[0], 0))
        glyph_obj.width = self.margins[0] + self.margins[1] + width


class MyGlyphA(MyGlyph):

    def __init__(self, codepoint: int, name: Optional[str],
                 width: Optional[int], margins: Tuple[int, int]) -> None:
        super().__init__(codepoint, name, width, margins)

    @abstractmethod
    def draw(self, pen: MyPen) -> None:
        pass

    def draw_on_glyph(self, glyph_obj: fontforge.glyph) -> None:
        pen = MyPen(glyph_obj.glyphPen(replace=False))
        self.draw(pen)


def str_to_margin_values(margins: str) -> Tuple[int, int]:
    return (MARGINSPEC[margins[0]], MARGINSPEC[margins[1]])


def glyph(codepoint: int = -1,
          name: Optional[str] = None,
          width: Optional[int] = None,
          pen: bool = True,
          margins: Tuple[int, int] | str = (MARGIN, MARGIN)):
    if codepoint == -1 and name is None:
        raise ValueError("A glyph must have either a codepoint or a name")

    if isinstance(margins, str):
        margins = str_to_margin_values(margins)

    def inner(f):

        class Glyph(MyGlyphA):

            def __init__(self) -> None:
                super().__init__(codepoint, name, width, margins)

            def draw(self, pen: MyPen) -> Polygon:
                return f(pen)

        class Glyph2(MyGlyph):

            def __init__(self) -> None:
                super().__init__(codepoint, name, width, margins)

            def draw_on_glyph(self, glyph: fontforge.glyph) -> Polygon:
                return f(glyph)

        return Glyph() if pen else Glyph2()

    return inner


@glyph(codepoint=0x20, width=2 * PPIX)
def glyph_space(pen: MyPen) -> None:
    pass


# For reasons, contextual substitution tables don't recognize the real
# ZWJ (U+200D),  and we can't use an OpenType `fina`` table to do our
# job since our codepoints are in the PUA.
# For this reason, Cenvos fonts have reserved U+E022 as a quasi-ZWJ
# for this purpose.
@glyph(codepoint=(ORIGIN + 0x22), name='c9zwj', width=0, margins='..')
def glyph_zwj(pen: MyPen) -> None:
    pass


@glyph(codepoint=(ORIGIN + 0x00), name='c9c', pen=False, margins='(-')
def glyph_c(glyph_obj: fontforge.glyph) -> None:
    glyph_gh.draw_on_glyph(glyph_obj)
    glyph_obj.activeLayer = "mask"
    pen = glyph_obj.glyphPen(replace=False)
    angle = math.radians(40.0)
    x = 2.25 * PPIX
    pen.moveTo((0, XHEIGHT / 2))
    pen.lineTo((x, XHEIGHT / 2 + x * math.tan(angle)))
    pen.lineTo((x, XHEIGHT / 2 - x * math.tan(angle)))
    pen.closePath()
    pen = None
    subtract_mask_layer(glyph_obj)


@glyph(name='c9cfinal', margins='/-')
def glyph_c_final(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(3 * PPIX, XHEIGHT - PPIX / 2), width=PPIX),
        StrokeElement(Point(0, XHEIGHT - PPIX / 2), width=PPIX),
        StrokeElement(Point(-2 * PPIX, -DESCENT), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x01), name='c9e', margins='-|')
def glyph_e(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, XHEIGHT), width=PPIX),
        StrokeElement(Point(0, -DESCENT), width=PPIX, lock='lhs', dim_by='y'),
        StrokeElement(Point(-4 * PPIX, -DESCENT + PPIX), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x02), name='c9n', pen=False, margins='((')
def glyph_n(glyph_obj: fontforge.glyph) -> None:
    pen = MyPen(glyph_obj.glyphPen(replace=False))

    # Representative values from the original vlina font
    upper_corner_stroke_angle = math.radians(34)
    lower_corner_stroke_angle = math.radians(15)
    diagonal_stroke_angle = math.radians(20)
    ho = PPIX * 3 / 4
    ho_into_bowl = PPIX / 4
    vo = PPIX * 3 / 4
    vo_inner = vo / 3
    ipa = 0
    lc = Point(1.875 * PPIX, 0.5 * PPIX)
    lco = Point.polar(0.5 * PPIX, math.pi / 2 + lower_corner_stroke_angle)
    lcb, lct = lc - lco, lc + lco
    lco2 = Point.polar(0.75 * PPIX, -math.pi + lower_corner_stroke_angle)
    lcb2, lct2 = lcb + lco2, lct + lco2
    rc = Point(-1.375 * PPIX, 3.5 * PPIX)
    rco = Point.polar(0.5 * PPIX, math.pi / 2 + upper_corner_stroke_angle)
    rcb, rct = rc - rco, rc + rco
    rco2 = Point.polar(0.625 * PPIX, upper_corner_stroke_angle)
    rcb2, rct2 = rcb + rco2, rct + rco2

    rbr = Point(-1.5 * PPIX, 1.25 * PPIX)
    ltr = Point(1.75 * PPIX, 3.25 * PPIX)

    cp = Point(0, XHEIGHT / 2)
    cpo = Point.polar(0.45 * PPIX, math.pi / 2 + diagonal_stroke_angle)
    cpb, cpt = cp - cpo, cp + cpo
    cpo2 = Point.polar(0.5 * PPIX, math.pi + diagonal_stroke_angle)
    # Bottom right (outer)
    pen.line_to(rbr.x, rbr.y)
    # Bottom point (outer)
    pen.curve_to(rbr.x, rbr.y - vo, -ho, -OVERSHOOT_ROUND, 0, -OVERSHOOT_ROUND)
    # Bottom left corner
    pen.curve_to(ho_into_bowl, -OVERSHOOT_ROUND, lcb2.x, lcb2.y, lcb.x, lcb.y)
    pen.line_to(lct.x, lct.y)
    pen.curve_to(lct2.x, lct2.y, ho, PPIX - OVERSHOOT_ROUND, 0,
                 PPIX - OVERSHOOT_ROUND)
    # Bottom right (inner)
    pen.curve_to(-ho_into_bowl, PPIX - OVERSHOOT_ROUND, rbr.x + PPIX,
                 rbr.y - ipa - vo_inner, rbr.x + PPIX, rbr.y - ipa)
    # Center (lower)
    pen.curve_to(rbr.x + PPIX, rbr.y - ipa + vo_inner, cpb.x + cpo2.x,
                 cpb.y + cpo2.y, cpb.x, cpb.y)
    # Top left (outer)
    pen.curve_to(cpb.x - cpo2.x, cpb.y - cpo2.y, ltr.x, ltr.y - vo, ltr.x,
                 ltr.y)
    # Top point (outer)
    pen.curve_to(ltr.x, ltr.y + vo, ho, XHEIGHT + OVERSHOOT_ROUND, 0,
                 XHEIGHT + OVERSHOOT_ROUND)
    # Top right corner
    pen.curve_to(-ho, XHEIGHT + OVERSHOOT_ROUND, rct2.x, rct2.y, rct.x, rct.y)
    pen.line_to(rcb.x, rcb.y)
    pen.curve_to(rcb2.x, rcb2.y, -ho_into_bowl,
                 XHEIGHT + OVERSHOOT_ROUND - PPIX, 0,
                 XHEIGHT + OVERSHOOT_ROUND - PPIX)
    # Top left (inner)
    pen.curve_to(ho_into_bowl, XHEIGHT + OVERSHOOT_ROUND - PPIX, ltr.x - PPIX,
                 ltr.y + ipa / 2 + vo_inner, ltr.x - PPIX, ltr.y + ipa / 2)
    # Center (upper)
    pen.curve_to(ltr.x - PPIX, ltr.y + ipa / 2 - vo_inner, cpt.x - cpo2.x,
                 cpt.y - cpo2.y, cpt.x, cpt.y)
    # Return to bottom right (outer)
    pen.curve_to(cpt.x + cpo2.x, cpt.y + cpo2.y, rbr.x, rbr.y + vo, rbr.x,
                 rbr.y)
    pen.close_path_reverse()

    pen = None
    glyph_obj.addExtrema()


@glyph(codepoint=(ORIGIN + 0x03), name='c9ng', margins='|-')
def glyph_ng(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(3 * PPIX, XHEIGHT - PPIX / 2), width=PPIX),
        StrokeElement(Point(0, XHEIGHT - PPIX / 2), width=PPIX),
        StrokeElement(Point(0, -DESCENT), width=PPIX),
    ])


@glyph(name='c9ngfinal', margins='--')
def glyph_ng_final(pen: MyPen) -> None:
    glyph_ng.draw(pen)
    stroke_character(pen, [
        StrokeElement(Point(2 * PPIX, 0.75 * PPIX),
                      width=PPIX,
                      angle=math.pi / 2,
                      fixation=1),
        StrokeElement(Point(-1.5 * PPIX, 0.25 * PPIX),
                      width=PPIX,
                      angle=math.pi / 2,
                      fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x04), name='c9v', margins='//')
def glyph_v(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0.5 * PPIX, 0), width=PPIX, fixation=1),
        StrokeElement(Point(2.25 * PPIX, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(
            Point(4 * PPIX, 0), width=PPIX, angle=math.pi, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x05), name='c9o', margins='/|')
def glyph_o(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, ASCENT), width=PPIX),
        StrokeElement(
            Point(0, -OVERSHOOT_ANGULAR), width=PPIX, lock='lhs', dim_by='y'),
        StrokeElement(
            Point(-3 * PPIX, O_HEIGHT), width=PPIX, lock='rhs', dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x06), name='c9s', pen=False, margins='-(')
def glyph_s(glyph_obj: fontforge.glyph) -> None:
    # iaj zav; xbsio la,
    # n v km l s zioa

    pen = MyPen(glyph_obj.glyphPen(replace=False))

    # Representative values from the original vlina font
    upper_corner_stroke_angle = math.radians(34)
    lower_corner_stroke_angle = math.radians(15)
    diagonal_stroke_angle = math.radians(20)
    ho = PPIX * 3 / 4
    ho_into_bowl = PPIX / 4
    vo = PPIX * 3 / 4
    vo_inner = vo / 3
    ipa = PPIX / 6
    lc = Point(-2 * PPIX, 0.5 * PPIX)
    lco = Point.polar(0.5 * PPIX, math.pi / 2 - lower_corner_stroke_angle)
    lcb, lct = lc - lco, lc + lco
    lco2 = Point.polar(0.75 * PPIX, -lower_corner_stroke_angle)
    lcb2, lct2 = lcb + lco2, lct + lco2
    rc = Point(1.5 * PPIX, 3.5 * PPIX)
    rco = Point.polar(0.5 * PPIX, math.pi / 2 - upper_corner_stroke_angle)
    rcb, rct = rc - rco, rc + rco
    rco2 = Point.polar(0.625 * PPIX, math.pi - upper_corner_stroke_angle)
    rcb2, rct2 = rcb + rco2, rct + rco2

    rbr = Point(1.5 * PPIX, 1.5 * PPIX)
    ltr = Point(-1.75 * PPIX, 3 * PPIX)

    cp = Point(0, XHEIGHT / 2)
    cpo = Point.polar(0.45 * PPIX, math.pi / 2 - diagonal_stroke_angle)
    cpb, cpt = cp - cpo, cp + cpo
    cpo2 = Point.polar(0.5 * PPIX, -diagonal_stroke_angle)
    # Bottom right (outer)
    pen.line_to(rbr.x, rbr.y)
    # Bottom point (outer)
    pen.curve_to(rbr.x, rbr.y - vo, ho, -OVERSHOOT_ROUND, 0, -OVERSHOOT_ROUND)
    # Bottom left corner
    pen.curve_to(-ho_into_bowl, -OVERSHOOT_ROUND, lcb2.x, lcb2.y, lcb.x, lcb.y)
    pen.line_to(lct.x, lct.y)
    pen.curve_to(lct2.x, lct2.y, -ho, PPIX - OVERSHOOT_ROUND, 0,
                 PPIX - OVERSHOOT_ROUND)
    # Bottom right (inner)
    pen.curve_to(ho_into_bowl, PPIX - OVERSHOOT_ROUND, rbr.x - PPIX,
                 rbr.y - ipa - vo_inner, rbr.x - PPIX, rbr.y - ipa)
    # Center (lower)
    pen.curve_to(rbr.x - PPIX, rbr.y - ipa + vo_inner, cpb.x + cpo2.x,
                 cpb.y + cpo2.y, cpb.x, cpb.y)
    # Top left (outer)
    pen.curve_to(cpb.x - cpo2.x, cpb.y - cpo2.y, ltr.x, ltr.y - vo, ltr.x,
                 ltr.y)
    # Top point (outer)
    pen.curve_to(ltr.x, ltr.y + vo, -ho, XHEIGHT + OVERSHOOT_ROUND, 0,
                 XHEIGHT + OVERSHOOT_ROUND)
    # Top right corner
    pen.curve_to(ho, XHEIGHT + OVERSHOOT_ROUND, rct2.x, rct2.y, rct.x, rct.y)
    pen.line_to(rcb.x, rcb.y)
    pen.curve_to(rcb2.x, rcb2.y, ho_into_bowl,
                 XHEIGHT + OVERSHOOT_ROUND - PPIX, 0,
                 XHEIGHT + OVERSHOOT_ROUND - PPIX)
    # Top left (inner)
    pen.curve_to(-ho_into_bowl, XHEIGHT + OVERSHOOT_ROUND - PPIX, ltr.x + PPIX,
                 ltr.y + ipa / 2 + vo_inner, ltr.x + PPIX, ltr.y + ipa / 2)
    # Center (upper)
    pen.curve_to(ltr.x + PPIX, ltr.y + ipa / 2 - vo_inner, cpt.x - cpo2.x,
                 cpt.y - cpo2.y, cpt.x, cpt.y)
    # Return to bottom right (outer)
    pen.curve_to(cpt.x + cpo2.x, cpt.y + cpo2.y, rbr.x, rbr.y + vo, rbr.x,
                 rbr.y)
    pen.close_path()

    pen = None
    glyph_obj.addExtrema()


@glyph(codepoint=(ORIGIN + 0x07), name='c9th', margins='//')
def glyph_th(pen: MyPen) -> None:
    for xoff in [0, 15 * PPIX / 8]:
        stroke_character(pen, [
            StrokeElement(Point(xoff + 0.5 * PPIX, XHEIGHT),
                          width=PPIX,
                          angle=math.pi,
                          fixation=1),
            StrokeElement(Point(xoff + 2.5 * PPIX, 0),
                          width=PPIX,
                          angle=math.pi,
                          fixation=1),
        ])


@glyph(codepoint=(ORIGIN + 0x08), name='c9sh', margins='|-')
def glyph_sh(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(3 * PPIX, XHEIGHT - PPIX / 2), width=PPIX),
        StrokeElement(Point(0, XHEIGHT - PPIX / 2), width=PPIX),
        StrokeElement(Point(0, 0), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x09), name='c9r', margins='/|')
def glyph_r(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, -DESCENT), width=PPIX),
        StrokeElement(Point(0, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='rhs',
                      dim_by='y'),
        StrokeElement(Point(-2.75 * PPIX, 0.25 * PPIX),
                      width=PPIX,
                      lock='rhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x0A), name='c9l', margins='x|')
def glyph_l(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, -DESCENT), width=PPIX),
        StrokeElement(Point(0, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='rhs',
                      dim_by='y'),
        StrokeElement(Point(-3.5 * PPIX, 0.25 * PPIX),
                      width=PPIX,
                      lock='rhs',
                      dim_by='xy'),
    ])
    stroke_character(pen, [
        StrokeElement(Point(0, -1.375 * PPIX),
                      width=PPIX * RL_EYE_THICKNESS_MULTI,
                      angle=math.pi / 2,
                      fixation=1),
        StrokeElement(Point(-3.125 * PPIX, 5 * PPIX),
                      width=PPIX,
                      lock='lhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x0B), name='c9lh', margins='x|')
def glyph_lh(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, ASCENT), width=PPIX),
        StrokeElement(
            Point(0, -OVERSHOOT_ANGULAR), width=PPIX, lock='lhs', dim_by='y'),
        StrokeElement(
            Point(-3 * PPIX, O_HEIGHT), width=PPIX, lock='rhs', dim_by='xy'),
    ])
    stroke_character(pen, [
        StrokeElement(
            Point(0, 4.5 * PPIX),
            width=PPIX * WEDGE_PEAK_THICKNESS_MULTI,
            angle=math.pi / 2,
            fixation=1,
        ),
        StrokeElement(Point(-2.75 * PPIX, -0.5 * PPIX),
                      width=PPIX,
                      lock='lhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x0C), name='c9m', margins='--')
def glyph_m(pen: MyPen) -> None:
    glyph_e.draw(pen)
    stroke_character(pen, [
        StrokeElement(Point(1.5 * PPIX, 2.25 * PPIX),
                      width=PPIX,
                      angle=math.pi / 2,
                      fixation=1),
        StrokeElement(Point(-2 * PPIX, 1.75 * PPIX),
                      width=PPIX,
                      angle=math.pi / 2,
                      fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x0D), name='c9a', margins='/x')
def glyph_a(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, XHEIGHT), width=PPIX, angle=math.pi,
                      fixation=1),
        StrokeElement(
            Point(-4 * PPIX, -DESCENT), width=PPIX, angle=math.pi, fixation=1),
    ])
    stroke_character(pen, [
        StrokeElement(
            Point(-2.5 * PPIX,
                  XHEIGHT), width=PPIX, angle=math.pi, fixation=1),
        StrokeElement(Point(0, 0), width=PPIX, angle=math.pi, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x0E), name='c9f', margins='//')
def glyph_f(pen: MyPen) -> None:
    for xoff in [0, 15 * PPIX / 8]:
        stroke_character(pen, [
            StrokeElement(Point(xoff + 2.5 * PPIX, XHEIGHT),
                          width=PPIX,
                          angle=math.pi,
                          fixation=1),
            StrokeElement(Point(xoff + 0.5 * PPIX, 0),
                          width=PPIX,
                          angle=math.pi,
                          fixation=1),
        ])


@glyph(codepoint=(ORIGIN + 0x0F), name='c9g', pen=False, margins='(|')
def glyph_g(glyph_obj: fontforge.glyph) -> None:
    glyph_gh.draw_on_glyph(glyph_obj)
    glyph_obj.activeLayer = "mask"
    pen = glyph_obj.glyphPen(replace=False)
    angle = math.radians(40.0)
    angle2 = math.radians(60.0)
    angle3 = math.radians(90 - 55.0)
    x = 2.25 * PPIX
    pen.moveTo((0, XHEIGHT / 2))
    pen.lineTo((x, XHEIGHT / 2 + x * math.tan(angle)))
    pen.lineTo((x, XHEIGHT / 2 - x * math.tan(angle2)))
    pen.closePath()
    pen = None
    subtract_mask_layer(glyph_obj)
    xs = [p for p in glyph_obj.foreground[0] if p.on_curve]
    xs.sort(key=lambda p: p.x)
    clx = xs[-4].x
    cly = xs[-4].y
    glyph_obj.activeLayer = "mask"
    pen = glyph_obj.glyphPen()
    pen.moveTo((clx, XHEIGHT / 2))
    pen.lineTo((x, XHEIGHT / 2))
    pen.lineTo((x, -DESCENT))
    pen.lineTo((clx, -DESCENT))
    pen.closePath()
    pen = None
    subtract_mask_layer(glyph_obj)
    pen = glyph_obj.glyphPen(replace=False)
    pen.moveTo((clx, cly))
    pen.curveTo((clx + 0.5 * PPIX, cly + 0.5 * PPIX * math.tan(angle3)),
                (clx + 0.5 * PPIX, cly + 0.5 * PPIX * math.tan(angle3)),
                (clx + PPIX, cly + 1.25 * PPIX * math.tan(angle3)))
    pen.lineTo((clx + PPIX, -DESCENT))
    pen.lineTo((clx, -DESCENT))
    pen.closePath()
    pen = None


@glyph(codepoint=(ORIGIN + 0x10), name='c9p', margins='|/')
def glyph_p(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, -DESCENT), width=PPIX),
        StrokeElement(Point(0, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(Point(2.75 * PPIX, 0.75 * PPIX),
                      width=PPIX,
                      lock='lhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x11), name='c9t', margins='//')
def glyph_t(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0.5 * PPIX, 0), width=PPIX, fixation=1),
        StrokeElement(Point(2.5 * PPIX, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_PLUS_LINE_THICKNESS_MULTI * PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(
            Point(4.5 * PPIX, 0), width=PPIX, angle=math.pi, fixation=1),
    ])
    stroke_character(pen, [
        StrokeElement(Point(2.5 * PPIX, -DESCENT), width=PPIX),
        StrokeElement(Point(2.5 * PPIX, XHEIGHT - PPIX), width=PPIX)
    ])


@glyph(codepoint=(ORIGIN + 0x12), name='c9ch', margins='x/')
def glyph_ch(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(-3 * PPIX, ASCENT), width=PPIX, angle=math.pi, fixation=1),
        StrokeElement(Point(0, 0), width=PPIX, angle=math.pi, fixation=1),
    ])
    stroke_character(pen, [
        StrokeElement(Point(-1.75 * PPIX, XHEIGHT - 0.375 * PPIX),
                      width=PPIX,
                      angle=math.pi,
                      fixation=1),
        StrokeElement(
            Point(-3.5 * PPIX, 0), width=PPIX, angle=math.pi, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x13), name='c9ix', margins='/-')
def glyph_ix(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(0, 0.5 * PPIX),
            width=PPIX,
        ),
        StrokeElement(
            Point(-3.5 * PPIX, 0.5 * PPIX),
            width=PPIX,
        ),
        StrokeElement(Point(-2.75 * PPIX, XHEIGHT + OVERSHOOT_ROUND),
                      width=PPIX,
                      lock='lhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x14), name='c9j', margins='//')
def glyph_j(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, XHEIGHT), width=PPIX, angle=math.pi,
                      fixation=1),
        StrokeElement(
            Point(-4 * PPIX, -DESCENT), width=PPIX, angle=math.pi, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x15), name='c9i', margins='||')
def glyph_i(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, 0), width=PPIX),
        StrokeElement(Point(0, ASCENT), width=PPIX)
    ])


@glyph(codepoint=(ORIGIN + 0x16), name='c9d', margins='xx')
def glyph_d(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(-3 * PPIX, XHEIGHT), width=PPIX, angle=math.pi, fixation=1),
        StrokeElement(Point(0, 0), width=PPIX, angle=math.pi, fixation=1),
    ])
    stroke_character(pen, [
        StrokeElement(Point(0, XHEIGHT), width=PPIX, angle=math.pi,
                      fixation=1),
        StrokeElement(
            Point(-3 * PPIX, 0), width=PPIX, angle=math.pi, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x17), name='c9dh', margins='xx')
def glyph_dh(pen: MyPen) -> None:
    for xoff in [0, 13 * PPIX / 8]:
        stroke_character(pen, [
            StrokeElement(Point(xoff + 0.5 * PPIX, XHEIGHT),
                          width=PPIX,
                          angle=math.pi,
                          fixation=1),
            StrokeElement(Point(xoff + 2.5 * PPIX, 0),
                          width=PPIX,
                          angle=math.pi,
                          fixation=1),
        ])
    stroke_character(pen, [
        StrokeElement(Point(4.125 * PPIX, XHEIGHT + OVERSHOOT_ROUND),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      angle=math.pi,
                      fixation=1),
        StrokeElement(Point(0.375 * PPIX, -OVERSHOOT_ROUND),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      angle=math.pi,
                      fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x18), name='c9h', margins='//')
def glyph_h(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(0.5 * PPIX, XHEIGHT), width=PPIX, angle=math.pi, fixation=1),
        StrokeElement(Point(2.25 * PPIX, -OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='rhs',
                      dim_by='y'),
        StrokeElement(Point(4 * PPIX, XHEIGHT), width=PPIX, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x19), name='c9gh', margins='((')
def glyph_gh(pen: MyPen) -> None:
    draw_oval(pen, -2.25 * PPIX, 2.25 * PPIX, -OVERSHOOT_ROUND,
              XHEIGHT + OVERSHOOT_ROUND, PPIX)


@glyph(codepoint=(ORIGIN + 0x1A), name='c9ex', margins='//')
def glyph_ex(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(-2 * PPIX, -DESCENT), width=PPIX, fixation=1),
        StrokeElement(Point(0.5 * PPIX, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=1 * PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(Point(2.25 * PPIX, -OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='rhs',
                      dim_by='y'),
        StrokeElement(Point(4 * PPIX, XHEIGHT), width=PPIX, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x1B), name='c9ox', margins='//')
def glyph_ox(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(1.75 * PPIX, -DESCENT), width=PPIX, fixation=1),
        StrokeElement(Point(3 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(0 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(1.25 * PPIX, XHEIGHT), width=PPIX, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x1C), name='c9ax', margins='//')
def glyph_ax(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(-4 * PPIX, XHEIGHT), width=PPIX, angle=math.pi, fixation=1),
        StrokeElement(
            Point(0, -DESCENT), width=PPIX, angle=math.pi, fixation=1),
    ])


@glyph(codepoint=(ORIGIN + 0x1D), name='c9u', margins='/|')
def glyph_u(pen: MyPen) -> None:
    glyph_o.draw(pen)
    for x_offset in [-2.75 * PPIX, -0.75 * PPIX]:
        draw_dot(pen, x_offset, -2 * PPIX)


@glyph(codepoint=(ORIGIN + 0x1E), name='c9gen', margins='<-')
def glyph_gen(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(8 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(4 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(3 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(0 * PPIX, 0.5 * PPIX), width=PPIX),
    ])
    draw_arrowhead(pen, 0, 0.5 * PPIX, math.pi)


@glyph(codepoint=(ORIGIN + 0x1F), name='c9tja', margins='<-')
def glyph_tja(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(8 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(4 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(3 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(0 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
    ])
    draw_arrowhead(pen, 0, MARKER_BASELINE + 0.5 * PPIX, math.pi)


@glyph(codepoint=(ORIGIN + 0x20), name='c9shac', margins='<-')
def glyph_shac(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(8 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(4 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(2 * PPIX, 0.75 * PPIX), width=PPIX),
        StrokeElement(Point(0 * PPIX, MARKER_BASELINE), width=PPIX),
    ])
    direction = math.atan2(MARKER_BASELINE - 0.75 * PPIX, -2 * PPIX)
    draw_arrowhead(pen, 0, MARKER_BASELINE, direction)


@glyph(codepoint=(ORIGIN + 0x21), name='c9cjar', margins='/-')
def glyph_cjar(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(8 * PPIX, MARKER_BASELINE - 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(2 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(0 * PPIX, -0.5 * PPIX), width=PPIX),
    ])
    direction = math.atan2(-(MARKER_BASELINE + PPIX), -2 * PPIX)
    draw_arrowhead(pen, 0, -0.5 * PPIX, direction)


@glyph(codepoint=(ORIGIN + 0x44), name='c9slash', pen=False, margins='==')
def glyph_slash(glyph_obj: fontforge.glyph) -> None:
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    points = [
        Point(0, 0),
        Point(0, 5 * PPIX),
        CurveControl(Point(0, 5 * PPIX), Point(0, ASCENT - 0.5 * PPIX),
                     Point(-0.75 * PPIX, ASCENT - 0.5 * PPIX)),
        CurveControl(Point(-1.25 * PPIX, ASCENT - 0.5 * PPIX),
                     Point(-2 * PPIX, 6.25 * PPIX), Point(-2 * PPIX,
                                                          5 * PPIX)),
        CurveControl(Point(-2 * PPIX, 4 * PPIX), Point(-1.5 * PPIX, 4 * PPIX),
                     Point(0, 4 * PPIX)),
        CurveControl(Point(1 * PPIX, 4 * PPIX), Point(1.625 * PPIX,
                                                      3.75 * PPIX),
                     Point(1.625 * PPIX, 2 * PPIX)),
        Point(1.625 * PPIX, 1 * PPIX)
    ]
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None

    glyph_obj.stroke("circular",
                     PPIX,
                     "butt",
                     "miter",
                     extrema=True,
                     removeoverlap='none')
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x23), name='c9hyphen', margins='--')
def glyph_hyphen(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(2.75 * PPIX, XHEIGHT + 0 * PPIX), width=PPIX),
        StrokeElement(Point(0 * PPIX, XHEIGHT - 1.5 * PPIX), width=PPIX),
        StrokeElement(Point(0 * PPIX, XHEIGHT + 0 * PPIX), width=PPIX),
        StrokeElement(Point(-2.875 * PPIX, XHEIGHT - 1.5 * PPIX), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x24), name='c9carth', margins='-|')
def glyph_carth(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(-3.5 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(0, MARKER_BASELINE + 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(0, 2 * MARKER_BASELINE - ASCENT + PPIX),
                      width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x25), name='c9tor', margins='-|')
def glyph_tor(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(-3.5 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(0, MARKER_BASELINE + 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(0, ASCENT), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x26), name='c9njor', margins='-|')
def glyph_njor(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(-5 * PPIX, MARKER_BASELINE + 0.5 * PPIX),
                      width=PPIX),
        StrokeElement(Point(PPIX, MARKER_BASELINE + 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(PPIX, ASCENT), width=PPIX),
    ])
    for rotation in [-1, 0, 1]:
        vertical(-0.5 * PPIX, -2.25 * PPIX, 2.25 * PPIX).transform(
            psMat.rotate(math.radians(45 * rotation))).translate(
                -2.75 * PPIX, MARKER_BASELINE + 0.5 * PPIX).trace_with_pen(pen)


@glyph(codepoint=(ORIGIN + 0x27), name='c9es', margins='//')
def glyph_es(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(0.5 * PPIX, XHEIGHT), width=PPIX, angle=math.pi, fixation=1),
        StrokeElement(Point(2.5 * PPIX, -OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='rhs',
                      dim_by='y'),
        StrokeElement(Point(4.5 * PPIX, XHEIGHT), width=PPIX, fixation=1),
    ])
    stroke_character(pen, [
        StrokeElement(Point(2.5 * PPIX, PPIX), width=PPIX),
        StrokeElement(Point(2.5 * PPIX, ASCENT), width=PPIX)
    ])


@glyph(codepoint=(ORIGIN + 0x28), name='c9nef', margins='--')
def glyph_nef(pen: MyPen) -> None:
    for rotation in [-1, 0, 1, 2]:
        vertical(-0.5 * PPIX, -2.5 * PPIX, 2.5 * PPIX).transform(
            psMat.rotate(math.radians(45 * rotation))).translate(
                0, MARKER_BASELINE).trace_with_pen(pen)


@glyph(codepoint=(ORIGIN + 0x29), name='c9ngos', margins='==')
def glyph_ngos(pen: MyPen) -> None:
    draw_dot(pen, 0, ASCENT / 2 - PPIX)


@glyph(codepoint=(ORIGIN + 0x2A), name='c9lhil', margins='==')
def glyph_lhil(pen: MyPen) -> None:
    draw_dot(pen, 0, 0)


@glyph(codepoint=(ORIGIN + 0x2B), name='c9rin', margins='||')
def glyph_rin(pen: MyPen) -> None:
    size = 3 * PPIX
    stroke_character(pen, [
        StrokeElement(Point(size - 0.5 * PPIX * math.sqrt(0.5),
                            ASCENT - 0.5 * PPIX * math.sqrt(0.5)),
                      width=PPIX),
        StrokeElement(Point(0, ASCENT - size), width=PPIX),
        StrokeElement(Point(0, ASCENT), width=PPIX)
    ])


@glyph(codepoint=(ORIGIN + 0x2C), name='c9cin', margins='||')
def glyph_cin(pen: MyPen) -> None:
    size = 3 * PPIX
    stroke_character(pen, [
        StrokeElement(Point(-size + 0.5 * PPIX * math.sqrt(0.5),
                            0.5 * PPIX * math.sqrt(0.5)),
                      width=PPIX),
        StrokeElement(Point(0, size), width=PPIX),
        StrokeElement(Point(0, 0), width=PPIX)
    ])


@glyph(codepoint=(ORIGIN + 0x2D), name='c9fos', pen=False, margins='||')
def glyph_fos(glyph_obj: fontforge.glyph) -> None:
    ho = 1.5 * PPIX
    hoc = 0.875 * PPIX
    vo = 0.5 * PPIX
    tooth2 = 21 / 32 * ASCENT
    tooth1 = ASCENT - tooth2
    limb2 = (2 * ASCENT + 3 * tooth2) / 5
    limb0 = 3 * tooth1 / 5
    limb1 = ASCENT / 2
    points = [
        Point(0, ASCENT + OVERSHOOT_ROUND - 0.5 * PPIX),
        CurveControl(Point(ho, ASCENT + OVERSHOOT_ROUND - 0.5 * PPIX),
                     Point(2 * PPIX, limb2 + vo), Point(2 * PPIX, limb2)),
        CurveControl(Point(2 * PPIX, limb2 - vo),
                     Point(0.125 * PPIX + hoc, tooth2),
                     Point(0.125 * PPIX, tooth2)),
        CurveControl(Point(0.125 * PPIX + hoc, tooth2),
                     Point(2 * PPIX, limb1 + vo), Point(2 * PPIX, limb1)),
        CurveControl(Point(2 * PPIX, limb1 - vo),
                     Point(0.125 * PPIX + hoc, tooth1),
                     Point(0.125 * PPIX, tooth1)),
        CurveControl(Point(0.125 * PPIX + hoc, tooth1),
                     Point(2 * PPIX, limb0 + vo), Point(2 * PPIX, limb0)),
        CurveControl(Point(2 * PPIX, limb0 - vo),
                     Point(ho, -OVERSHOOT_ROUND + 0.5 * PPIX),
                     Point(0, -OVERSHOOT_ROUND + 0.5 * PPIX)),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    glyph_obj.stroke("circular", PPIX, "butt", "miter", extrema=True)


@glyph(codepoint=(ORIGIN + 0x2E), name='c9thos', margins='||')
def glyph_thos(pen: MyPen) -> None:
    points = [
        StrokeElement(Point(-2 * PPIX, ASCENT / 2), width=PPIX),
        StrokeElement(Point(0, ASCENT + OVERSHOOT_ANGULAR),
                      width=PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(Point(0, -OVERSHOOT_ANGULAR),
                      width=PPIX,
                      lock='lhs',
                      dim_by='y'),
    ]
    stroke_character(pen, [*points, *points, points[0]])


@glyph(codepoint=(ORIGIN + 0x2F), name='c9sen', pen=False, margins='<-')
def glyph_sen(glyph_obj: fontforge.glyph) -> None:
    section_width = 10 * PPIX / 11
    ho = 2 * section_width / 3
    points = [
        Point(11 * section_width, ASCENT / 2),
        Point(9.5 * section_width, ASCENT / 2),
        CurveControl(Point(9.5 * section_width - ho, ASCENT / 2),
                     Point(8 * section_width + ho, ASCENT - 0.5 * PPIX),
                     Point(8 * section_width, ASCENT - 0.5 * PPIX)),
        CurveControl(Point(8 * section_width - ho, ASCENT - 0.5 * PPIX),
                     Point(6 * section_width + ho, 0.5 * PPIX),
                     Point(6 * section_width, 0.5 * PPIX)),
        CurveControl(Point(6 * section_width - ho, 0.5 * PPIX),
                     Point(4 * section_width + ho, ASCENT - 0.5 * PPIX),
                     Point(4 * section_width, ASCENT - 0.5 * PPIX)),
        CurveControl(Point(4 * section_width - ho, ASCENT - 0.5 * PPIX),
                     Point(2 * section_width + ho, 0.5 * PPIX),
                     Point(2 * section_width, 0.5 * PPIX)),
        CurveControl(Point(2 * section_width - ho, 0.5 * PPIX),
                     Point(1 * section_width + ho, ASCENT / 2),
                     Point(1 * section_width, ASCENT / 2)),
        Point(0, ASCENT / 2),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj)
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    draw_arrowhead(pen, 0, ASCENT / 2, -math.pi)
    pen = None


@glyph(codepoint=(ORIGIN + 0x45), name='c9vas', margins='</')
def glyph_vas(pen: MyPen) -> None:
    size = 3 * PPIX
    stroke_character(pen, [
        StrokeElement(Point(1.5 * PPIX, 0.5 * ASCENT + 2 * PPIX), width=PPIX),
        StrokeElement(Point(0, 0.5 * ASCENT), width=PPIX),
        StrokeElement(Point(1.5 * PPIX, 0.5 * ASCENT - 2 * PPIX), width=PPIX)
    ])


@glyph(codepoint=(ORIGIN + 0x30), name='c9zero', width=5 * PPIX, pen=False)
def glyph_0(glyph_obj: fontforge.glyph) -> None:
    ho = PPIX
    bowl_height = 3.75 * PPIX
    bowl_edge_height = 2.25 * PPIX
    points = [
        Point(3.5 * PPIX, ASCENT - 0.5 * PPIX),
        CurveControl(
            Point(3 * PPIX - ho / 2, ASCENT - 0.5 * PPIX),
            Point(0.5 * PPIX, bowl_height + 2 * PPIX),
            Point(0.5 * PPIX, bowl_edge_height),
        ),
        CurveControl(Point(0.5 * PPIX, 1 * PPIX),
                     Point(2.5 * PPIX - ho, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(2.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND)),
        CurveControl(Point(2.5 * PPIX + ho, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(4.5 * PPIX, 1 * PPIX),
                     Point(4.5 * PPIX, bowl_edge_height)),
        CurveControl(Point(4.5 * PPIX, bowl_height),
                     Point(2.5 * PPIX + ho, bowl_height),
                     Point(2.5 * PPIX, bowl_height)),
        CurveControl(Point(2.5 * PPIX - ho, bowl_height),
                     Point(1.25 * PPIX, 3.25 * PPIX),
                     Point(0.475 * PPIX, 3 * PPIX))
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x31), name='c9one', width=5 * PPIX, pen=False)
def glyph_1(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(4.5 * PPIX, 6.75 * PPIX),
        Point(0.5 * PPIX, 0 * PPIX),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x32), name='c9two', width=5 * PPIX, pen=False)
def glyph_2(glyph_obj: fontforge.glyph) -> None:
    ho = PPIX
    bowl_height = 2.75 * PPIX
    bowl_edge_height = 1.75 * PPIX
    points = [
        Point(4.5 * PPIX, 6.75 * PPIX),
        CurveControl(
            Point(2.5 * PPIX, 4.25 * PPIX),
            Point(1 * PPIX, bowl_height),
            Point(1 * PPIX, bowl_edge_height),
        ),
        CurveControl(Point(1 * PPIX, 1 * PPIX),
                     Point(2.5 * PPIX - ho, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(2.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND)),
        CurveControl(Point(2.5 * PPIX + ho, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(4 * PPIX, 1 * PPIX),
                     Point(4 * PPIX, bowl_edge_height)),
        CurveControl(Point(4 * PPIX, bowl_height),
                     Point(2.5 * PPIX, 4.25 * PPIX),
                     Point(0.5 * PPIX, 6.75 * PPIX)),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x33), name='c9three', width=5 * PPIX, pen=False)
def glyph_3(glyph_obj: fontforge.glyph) -> None:
    ho = PPIX
    points = [
        Point(1.0 * PPIX, 3.75 * PPIX),
        CurveControl(
            Point(0.875 * PPIX, 4.0 * PPIX),
            Point(0.75 * PPIX, 4.5 * PPIX),
            Point(0.75 * PPIX, 5 * PPIX),
        ),
        CurveControl(
            Point(0.75 * PPIX, 5.75 * PPIX),
            Point(2.75 * PPIX - ho, ASCENT - 0.5 * PPIX),
            Point(2.75 * PPIX, ASCENT - 0.5 * PPIX),
        ),
        CurveControl(
            Point(2.75 * PPIX + ho, ASCENT - 0.5 * PPIX),
            Point(4.25 * PPIX, 5.5 * PPIX),
            Point(4.25 * PPIX, 5.0 * PPIX),
        ),
        CurveControl(
            Point(4.25 * PPIX, 4.5 * PPIX),
            Point(4.0 * PPIX, 3.0 * PPIX),
            Point(3.5 * PPIX, 0.25 * PPIX),
        ),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x34), name='c9four', width=5 * PPIX)
def glyph_4(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(4.5 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(4.5 * PPIX, ASCENT), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x35), name='c9five', width=5 * PPIX)
def glyph_5(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(0, ASCENT - 2.5 * PPIX), width=PPIX),
        StrokeElement(Point(5 * PPIX, ASCENT - 2.5 * PPIX), width=PPIX),
    ])
    stroke_character(pen, [
        StrokeElement(Point(2.5 * PPIX, 0), width=PPIX),
        StrokeElement(Point(2.5 * PPIX, ASCENT), width=PPIX),
    ])


@glyph(codepoint=(ORIGIN + 0x36), name='c9six', width=5 * PPIX, pen=False)
def glyph_6(glyph_obj: fontforge.glyph) -> None:
    ho = PPIX
    bowl_height = 4.25 * PPIX
    bowl_edge_height = 2.25 * PPIX
    eye_height = 2.25 * PPIX
    points = [
        Point(1.5 * PPIX, ASCENT - 0.5 * PPIX),
        CurveControl(
            Point(2 * PPIX + ho / 2, ASCENT - 0.5 * PPIX),
            Point(4.5 * PPIX, bowl_height + 1.5 * PPIX),
            Point(4.5 * PPIX, bowl_edge_height),
        ),
        CurveControl(Point(4.5 * PPIX, 1 * PPIX),
                     Point(2.5 * PPIX + ho, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(2.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND)),
        CurveControl(Point(2.5 * PPIX - ho, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(0.5 * PPIX, 1 * PPIX),
                     Point(0.5 * PPIX, bowl_edge_height)),
        CurveControl(Point(0.5 * PPIX, bowl_height),
                     Point(2 * PPIX - ho / 2, bowl_height),
                     Point(2 * PPIX, bowl_height)),
        CurveControl(Point(2 * PPIX + ho / 3, bowl_height),
                     Point(2.875 * PPIX, bowl_height - 0.5 * PPIX),
                     Point(2.875 * PPIX, eye_height)),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x37), name='c9seven', width=5 * PPIX, pen=False)
def glyph_7(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(0.5 * PPIX, XHEIGHT - 2 * PPIX),
        CurveControl(Point(1 * PPIX, XHEIGHT - 1.5 * PPIX),
                     Point(2 * PPIX, XHEIGHT + OVERSHOOT_ROUND - 0.5 * PPIX),
                     Point(3 * PPIX, XHEIGHT + OVERSHOOT_ROUND - 0.5 * PPIX)),
        CurveControl(Point(4 * PPIX, XHEIGHT + OVERSHOOT_ROUND - 0.5 * PPIX),
                     Point(4.5 * PPIX, 3.5 * PPIX),
                     Point(4.5 * PPIX, 2 * PPIX)),
        Point(4.5 * PPIX, 0)
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    vertical_line(pen, 0, 0, ASCENT)
    pen = None


@glyph(codepoint=(ORIGIN + 0x38), name='c9eight', width=5 * PPIX)
def glyph_8(pen: MyPen) -> None:
    draw_oval(pen, 0, 5 * PPIX, 0.5 * PPIX, ASCENT - 0.5 * PPIX)


@glyph(codepoint=(ORIGIN + 0x39), name='c9nine', width=5 * PPIX, pen=False)
def glyph_9(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(4.5 * PPIX, ASCENT),
        Point(4.5 * PPIX, 0.5 * PPIX),
        Point(1.5 * PPIX, 0.5 * PPIX),
        CurveControl(
            Point(0.5 * PPIX, 0.5 * PPIX),
            Point(0.5 * PPIX, 1.25 * PPIX),
            Point(0.5 * PPIX, 1.75 * PPIX),
        ),
        CurveControl(Point(0.5 * PPIX, 2.75 * PPIX),
                     Point(1.25 * PPIX, 2.75 * PPIX),
                     Point(1.5 * PPIX, 2.75 * PPIX)),
        CurveControl(Point(2 * PPIX, 2.75 * PPIX), Point(2.5 * PPIX, 2 * PPIX),
                     Point(2.5 * PPIX, 0.25 * PPIX)),
        CurveControl(Point(2.5 * PPIX, -1.5 * PPIX),
                     Point(2.0 * PPIX, -DESCENT + 1.375 * PPIX),
                     Point(1.5 * PPIX, -DESCENT + 1 * PPIX))
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x3A), name='c9ten', width=5 * PPIX, pen=False)
def glyph_A(glyph_obj: fontforge.glyph) -> None:
    ho = PPIX
    vo = 1.25 * PPIX
    bowl_height = 4 * PPIX
    bowl_edge_height = 2.375 * PPIX
    points = [
        Point(0.75 * PPIX, ASCENT),
        Point(0.75 * PPIX, 3.25 * PPIX),
        CurveControl(
            Point(1.5 * PPIX, 3.875 * PPIX),
            Point(2.5 * PPIX - ho / 2, bowl_height),
            Point(2.5 * PPIX, bowl_height),
        ),
        CurveControl(
            Point(2.5 * PPIX + ho, bowl_height),
            Point(4.5 * PPIX, bowl_edge_height + vo),
            Point(4.5 * PPIX, bowl_edge_height),
        ),
        CurveControl(
            Point(4.5 * PPIX, bowl_edge_height - vo),
            Point(2.5 * PPIX + ho, 0.5 * PPIX - OVERSHOOT_ROUND),
            Point(2.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND),
        ),
        CurveControl(
            Point(2.5 * PPIX - ho, 0.5 * PPIX - OVERSHOOT_ROUND),
            Point(1 * PPIX, 0.75 * PPIX),
            Point(0.25 * PPIX, 1 * PPIX),
        )
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x3B), name='c9eleven', width=5 * PPIX, pen=False)
def glyph_B(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(2.5 * PPIX, 0),
        Point(2.5 * PPIX, 3.25 * PPIX),
        Point(0.5 * PPIX, 3 * PPIX),
        Point(0.5 * PPIX, 4.75 * PPIX),
        CurveControl(Point(0.5 * PPIX, 5.75 * PPIX),
                     Point(1.5 * PPIX, ASCENT - 0.5 * PPIX),
                     Point(2.5 * PPIX, ASCENT - 0.5 * PPIX)),
        CurveControl(Point(3.5 * PPIX, ASCENT - 0.5 * PPIX),
                     Point(4.5 * PPIX, 5.5 * PPIX),
                     Point(4.5 * PPIX, 4 * PPIX)),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x3C), name='c9twelve', width=5 * PPIX, pen=False)
def glyph_C(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(4.25 * PPIX, 6.75 * PPIX),
        Point(0.75 * PPIX, 6.75 / 2 * PPIX),
        Point(4.25 * PPIX, 0 * PPIX),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x3D), name='c9thirteen', width=5 * PPIX, pen=False)
def glyph_D(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(5 * PPIX, ASCENT - 0.5 * PPIX),
        Point(1.125 * PPIX, ASCENT - 0.5 * PPIX),
        CurveControl(Point(1.625 * PPIX, ASCENT - 1 * PPIX),
                     Point(2 * PPIX, 5.5 * PPIX), Point(2.5 * PPIX, 5 * PPIX)),
        CurveControl(Point(3 * PPIX, 4.5 * PPIX), Point(4.5 * PPIX, 3 * PPIX),
                     Point(4.5 * PPIX, 2 * PPIX)),
        CurveControl(Point(4.5 * PPIX, 1 * PPIX),
                     Point(3.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND),
                     Point(2.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND)),
        CurveControl(
            Point(1.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND),
            Point(1 * PPIX, 1 * PPIX),
            Point(0.25 * PPIX, 1.5 * PPIX),
        )
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x3E), name='c9fourteen', width=5 * PPIX, pen=False)
def glyph_E(glyph_obj: fontforge.glyph) -> None:
    points = [
        Point(0.5 * PPIX, ASCENT - 0.25 * PPIX),
        CurveControl(Point(0.5 * PPIX, 4.5 * PPIX),
                     Point(0.5 * PPIX, 3.5 * PPIX),
                     Point(2.5 * PPIX, 3.5 * PPIX)),
        CurveControl(Point(3.5 * PPIX,
                           3.5 * PPIX), Point(4.5 * PPIX, 4 * PPIX),
                     Point(4.5 * PPIX, 5.5 * PPIX)),
        CurveControl(Point(4.5 * PPIX, 6 * PPIX),
                     Point(4 * PPIX, ASCENT - 0.5 * PPIX),
                     Point(3.5 * PPIX, ASCENT - 0.5 * PPIX)),
        CurveControl(Point(2.5 * PPIX, ASCENT - 0.5 * PPIX),
                     Point(2.5 * PPIX, ASCENT - 0.5 * PPIX),
                     Point(2.5 * PPIX, 0)),
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x3F), name='c9fifteen', width=5 * PPIX, pen=False)
def glyph_F(glyph_obj: fontforge.glyph) -> None:
    ho = PPIX
    vo = 1.25 * PPIX
    bowl_height = 4 * PPIX
    bowl_edge_height = 2.25 * PPIX
    points = [
        Point(4.0 * PPIX, ASCENT),
        Point(4.0 * PPIX, 3.5 * PPIX),
        CurveControl(
            Point(3.5 * PPIX, 3.75 * PPIX),
            Point(2.5 * PPIX + ho / 2, bowl_height),
            Point(2.5 * PPIX, bowl_height),
        ),
        CurveControl(
            Point(2.5 * PPIX - ho, bowl_height),
            Point(0.5 * PPIX, bowl_edge_height + vo),
            Point(0.5 * PPIX, bowl_edge_height),
        ),
        CurveControl(
            Point(0.5 * PPIX, bowl_edge_height - vo),
            Point(2.5 * PPIX - ho, 0.5 * PPIX - OVERSHOOT_ROUND),
            Point(2.5 * PPIX, 0.5 * PPIX - OVERSHOOT_ROUND),
        ),
        CurveControl(
            Point(2.5 * PPIX + ho, 0.5 * PPIX - OVERSHOOT_ROUND),
            Point(4 * PPIX, 0.75 * PPIX),
            Point(4.625 * PPIX, 1.125 * PPIX),
        )
    ]
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None
    ff_stroke(glyph_obj, remove_overlap=False)
    glyph_obj.correctDirection()


@glyph(codepoint=(ORIGIN + 0x40), name='c9w', margins='|/')
def glyph_w(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(
            Point(0, -DESCENT),
            width=PPIX,
        ),
        StrokeElement(
            Point(0, 0.5 * PPIX),
            width=PPIX,
        ),
        StrokeElement(
            Point(3 * PPIX, 0.5 * PPIX),
            width=PPIX,
        ),
        StrokeElement(Point(2.25 * PPIX, XHEIGHT + OVERSHOOT_ROUND),
                      width=PPIX,
                      lock='rhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x41), name='c9x', pen=False, margins='--')
def glyph_x(glyph_obj: fontforge.glyph) -> None:
    tilde_start = -1.75 * PPIX
    tilde_hw = 2.25 * PPIX
    tilde_ho = 0.875 * PPIX
    tilde_y = XHEIGHT + 0.875 * PPIX
    tilde_amp = 0.625 * PPIX
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    points = [
        Point(tilde_start, tilde_y),
        CurveControl(
            Point(tilde_start - tilde_ho, tilde_y + tilde_amp),
            Point(tilde_start - tilde_hw + tilde_ho, tilde_y + tilde_amp),
            Point(tilde_start - tilde_hw, tilde_y)),
        CurveControl(
            Point(tilde_start - tilde_hw - tilde_ho, tilde_y - tilde_amp),
            Point(tilde_start - 2 * tilde_hw + tilde_ho, tilde_y - tilde_amp),
            Point(tilde_start - 2 * tilde_hw, tilde_y)),
    ]
    for p in points:
        pen.add(p)
    pen.open_path()
    pen = None

    ff_stroke(glyph_obj)

    pen = MyPen(glyph_obj.glyphPen(replace=False))
    stroke_character(pen, [
        StrokeElement(Point(0, XHEIGHT - 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(-3.5 * PPIX, XHEIGHT - 0.5 * PPIX), width=PPIX),
        StrokeElement(
            Point(-PPIX, -DESCENT), width=PPIX, lock='rhs', dim_by='y'),
    ])
    pen = None


@glyph(codepoint=(ORIGIN + 0x42), name='c9y', margins='-/')
def glyph_y(pen: MyPen) -> None:
    stroke_character(pen, [
        StrokeElement(Point(-3 * PPIX, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(0, 0.5 * PPIX), width=PPIX),
        StrokeElement(Point(0, XHEIGHT + OVERSHOOT_ANGULAR),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(Point(2.75 * PPIX, 0.75 * PPIX),
                      width=PPIX,
                      lock='lhs',
                      dim_by='xy'),
    ])


@glyph(codepoint=(ORIGIN + 0x43), name='c9z', margins='//')
def glyph_z(pen: MyPen) -> None:
    min_y = -DESCENT
    max_y = XHEIGHT + OVERSHOOT_ANGULAR
    avg_y = (min_y + max_y) / 2
    stroke_character(pen, [
        StrokeElement(Point(2.5 * PPIX, avg_y + PPIX), width=PPIX, lock='rhs'),
        StrokeElement(Point(0, min_y),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='lhs',
                      dim_by='y'),
        StrokeElement(Point(0, max_y),
                      width=WEDGE_PEAK_THICKNESS_MULTI * PPIX,
                      lock='rhs',
                      dim_by='y'),
        StrokeElement(Point(-2.5 * PPIX, avg_y - PPIX), width=PPIX,
                      lock='lhs'),
    ])


def draw_macron(pen: MyPen):
    stroke_character(pen, [
        StrokeElement(Point(0.5 * PPIX, XHEIGHT + 1.5 * PPIX), width=PPIX),
        StrokeElement(Point(-3.5 * PPIX, XHEIGHT + 1.5 * PPIX), width=PPIX),
    ])


def draw_circumflex(pen: MyPen):
    stroke_character(pen, [
        StrokeElement(Point(-0.5 * PPIX, XHEIGHT + 0.25 * PPIX), width=PPIX),
        StrokeElement(Point(-2 * PPIX, XHEIGHT + 1.75 * PPIX), width=PPIX),
        StrokeElement(Point(-3.5 * PPIX, XHEIGHT + 0.25 * PPIX), width=PPIX),
    ])


@glyph(name='c9ee', margins='-|')
def ligature_ee(pen: MyPen) -> None:
    glyph_e.draw(pen)
    draw_macron(pen)


@glyph(name='c9em', margins='-|')
def ligature_em(pen: MyPen) -> None:
    glyph_e.draw(pen)
    draw_circumflex(pen)


@glyph(name='c9me', margins='-|')
def ligature_me(pen: MyPen) -> None:
    glyph_m.draw(pen)
    draw_macron(pen)


@glyph(name='c9mm', margins='-|')
def ligature_mm(pen: MyPen) -> None:
    glyph_m.draw(pen)
    draw_circumflex(pen)


def draw_circle(pen: MyPen, x: float, y: float, r: float, thickness: float):
    draw_oval(pen, x - r, x + r, y - r, y + r, thickness)


@glyph(name='c9jax', margins='(/')
def ligature_jax(pen: MyPen) -> None:
    glyph_j.draw(pen)
    draw_circle(pen, -3.625 * PPIX, XHEIGHT - PPIX, 2.875 * PPIX / 2,
                0.875 * PPIX)


@glyph(name='c9axj', margins='(/')
def ligature_axj(pen: MyPen) -> None:
    glyph_ax.draw(pen)
    draw_circle(pen, -3.9375 * PPIX, -DESCENT + 1.5 * PPIX, 2.875 * PPIX / 2,
                0.875 * PPIX)


@glyph(name='c9ww', margins='(/')
def ligature_ww(pen: MyPen) -> None:
    glyph_w.draw(pen)
    draw_circle(pen, -0.75 * PPIX, XHEIGHT - PPIX, 2.875 * PPIX / 2,
                0.875 * PPIX)


@glyph(name='c9xx', margins='/-', pen=False)
def ligature_xx(glyph_obj: fontforge.glyph) -> None:
    glyph_x.draw_on_glyph(glyph_obj)
    pen = MyPen(glyph_obj.glyphPen(replace=False))
    for offset in [
            Point(-3.875 * PPIX, 0.5 * PPIX),
            Point(-5.375 * PPIX, 1.5 * PPIX)
    ]:
        stroke_character(pen, [
            StrokeElement(offset, width=PPIX),
            StrokeElement(offset + Point(-1.25 * PPIX, -2.5 * PPIX),
                          width=PPIX),
        ])


@glyph(name='c9yy', margins='-/')
def ligature_yy(pen: MyPen) -> None:
    glyph_y.draw(pen)
    stroke_character(pen, [
        StrokeElement(Point(-1.75 * PPIX, XHEIGHT + 1.5 * PPIX), width=PPIX),
        StrokeElement(Point(-1.75 * PPIX, XHEIGHT - PPIX), width=PPIX),
    ])


@glyph(name='c9zz', margins='(/')
def ligature_zz(pen: MyPen) -> None:
    glyph_z.draw(pen)
    draw_circle(pen, -2 * PPIX, XHEIGHT, 2.875 * PPIX / 2, 0.875 * PPIX)


if __name__ == '__main__':
    os.makedirs('output/', exist_ok=True)
    try:
        os.remove("output/NafaVlina.otf")
    except FileNotFoundError:
        pass
    print("Generating the font...")
    font = fontforge.font()
    font.fontname = 'NafaVlina'
    font.familyname = 'NafaVlina'
    font.fullname = 'NafaVlina'
    font.ascent = ASCENT
    font.descent = DESCENT

    font.layers.add("mask", False, True)

    # Add glyphs

    glyph_space.generate(font)
    glyph_zwj.generate(font)
    glyph_c.generate(font)
    glyph_c_final.generate(font)
    glyph_e.generate(font)
    glyph_n.generate(font)
    glyph_ng.generate(font)
    glyph_ng_final.generate(font)
    glyph_v.generate(font)
    glyph_o.generate(font)
    glyph_s.generate(font)
    glyph_th.generate(font)
    glyph_sh.generate(font)
    glyph_r.generate(font)
    glyph_l.generate(font)
    glyph_lh.generate(font)
    glyph_m.generate(font)
    glyph_a.generate(font)
    glyph_f.generate(font)
    glyph_g.generate(font)
    glyph_p.generate(font)
    glyph_t.generate(font)
    glyph_ch.generate(font)
    glyph_ix.generate(font)
    glyph_j.generate(font)
    glyph_i.generate(font)
    glyph_d.generate(font)
    glyph_dh.generate(font)
    glyph_h.generate(font)
    glyph_gh.generate(font)
    glyph_ex.generate(font)
    glyph_ox.generate(font)
    glyph_ax.generate(font)
    glyph_u.generate(font)
    glyph_gen.generate(font)
    glyph_tja.generate(font)
    glyph_shac.generate(font)
    glyph_cjar.generate(font)
    glyph_slash.generate(font)
    glyph_hyphen.generate(font)
    glyph_carth.generate(font)
    glyph_tor.generate(font)
    glyph_njor.generate(font)
    glyph_es.generate(font)
    glyph_nef.generate(font)
    glyph_ngos.generate(font)
    glyph_lhil.generate(font)
    glyph_rin.generate(font)
    glyph_cin.generate(font)
    glyph_fos.generate(font)
    glyph_thos.generate(font)
    glyph_sen.generate(font)
    glyph_vas.generate(font)
    glyph_0.generate(font)
    glyph_1.generate(font)
    glyph_2.generate(font)
    glyph_3.generate(font)
    glyph_4.generate(font)
    glyph_5.generate(font)
    glyph_6.generate(font)
    glyph_7.generate(font)
    glyph_8.generate(font)
    glyph_9.generate(font)
    glyph_A.generate(font)
    glyph_B.generate(font)
    glyph_C.generate(font)
    glyph_D.generate(font)
    glyph_E.generate(font)
    glyph_F.generate(font)
    glyph_w.generate(font)
    glyph_x.generate(font)
    glyph_y.generate(font)
    glyph_z.generate(font)
    ligature_ee.generate(font)
    ligature_em.generate(font)
    ligature_me.generate(font)
    ligature_mm.generate(font)
    ligature_jax.generate(font)
    ligature_axj.generate(font)
    ligature_ww.generate(font)
    ligature_xx.generate(font)
    ligature_yy.generate(font)
    ligature_zz.generate(font)

    font.mergeFeature('output/features-nv.fea')

    font.autoHint()

    font.encoding = 'Unicode'
    font.encoding = 'compacted'

    # Validate and export

    errors = font.validate()
    if errors != 0:
        print(f"Found errors (num = {errors})", file=stderr)

    font.save('output/NafaVlina.sfd')
    font.generate('output/NafaVlina.otf')

    # Generate auxiliary file
    with open("output/mapping.json", "w") as mapping_file:
        mapping = {c: chr(ORIGIN + i) for i, c in enumerate(ROMANIZATION)}
        mapping[' '] = ' '
        json.dump(mapping, mapping_file)
